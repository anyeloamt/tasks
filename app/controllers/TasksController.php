<?php

use Amt\Services\TaskCreatorService;
use Amt\Validators\ValidationException;

/**
 * Tasks controller
 */
class TasksController extends \BaseController {

	/**
	 * Task creator service instance
	 * @var Amt\Services\TaskCreatorService
 	*/
	protected $taskCreator;

	public function __construct(TaskCreatorService $taskCreator)
	{
		$this->taskCreator = $taskCreator;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// fetch all tasks
		$tasks = Task::with('user')->get();

		// Users to the dropdown form
		$users = User::lists('username', 'id');

		// load a view to display them
		return View::make('tasks.index', compact('tasks', 'users'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		try
		{
			$this->taskCreator->make(Input::all());
		} 

		catch(ValidationException $e)
		{
			return Redirect::back()->withInput()->withErrors($e->getErrors());
		}

		return Redirect::home();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// fetch task with this id
		$task = Task::findOrFail($id);
		// load a view to display it
		return View::make('tasks.show', compact('task'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$task = Task::find($id);
		$task->completed = Input::get('completed') ?: 0;
		$task->save();

		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}