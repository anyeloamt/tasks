<?php
/**
 * Returns a url to gravatar
 * @param  string $email Gravatar user email
 * @return string Gravatar image url
 */
function gravatar_url($email)
{
	return "http://www.gravatar.com/avatar/" . md5($email) . "?s=40";
}
/**
 * Returns a gravatar image
 * @param  string $email Gravatar user email
 * @return string Html img tag
 */
function gravatar_tag(Task $task)
{
	$url = gravatar_url($task->user->email);
	return "<a href='/{$task->user->username}/tasks'><img src='{$url}' alt='{$task->user->email}'/></a>";
}
/**
 * Returns a link to a task
 * @param  Task $task Task to display
 * @return string Html a tag
 */
function link_to_task(Task $task)
{
	return link_to_route("user.tasks.show", $task->title, array($task->user->username, $task->id));
}
/**
 * Returns a task tag with an image and link to a task
 * @param  Task   $task Task to display
 * @return string Html stuff
 */
function task_tag(Task $task)
{
	return gravatar_tag($task) . link_to_task($task);
}