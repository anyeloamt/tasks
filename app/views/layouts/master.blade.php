<!doctype html>
<html>
	<head>
		<title>All tasks</title>
		<link rel="stylesheet" href="/css/bootstrap.css">
		<style>
			span.errors{ color: #db5463; font-style: italic;}
			.completed {background-color: #89e78c; border-bottom: 1px solid #585e58 !important;}
			.task-update-form {position: absolute; top: 1em; right: 1em;}
		</style>
	</head>
	<body>
		<div class="container">
			@yield('content')
		</div>

		<script src="/js/jquery.js"></script>
		@yield('scripts')
	</body>
</html>