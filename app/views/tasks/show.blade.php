@extends('layouts.master')

@section('content')
	<h1>{{ $task->title }} </h1>
	<article>
		{{ nl2br($task->body) }}
	</article>

	<p> {{ link_to_route('home', 'Regresar') }}</p>
@stop