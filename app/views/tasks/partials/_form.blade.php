{{ Form::open(array('url' => '/tasks', 'role' => 'form')) }}
	<fieldset>
		<div class="form-group">
			{{ Form::label('assign', 'Assign to:', array('class' => 'control-label')) }}
			{{ Form::select('assign', $users, null, array('class' => 'form-control')) }}
		</div>
		<div class="form-group">
			{{ Form::label('title', 'Title:', array('class' => 'control-label')) }}
			{{ Form::text('title', null , array('class' => 'form-control')) }}
			{{ $errors->first('body', '<span class="errors">:message</span>') }}
		</div>
		<div class="form-group">
			{{ Form::label('body', 'Body:', array('class' => 'control-label')) }}
			{{ Form::textarea('body', null , array('class' => 'form-control')) }}
			{{ $errors->first('title', '<span class="errors">:message</span>') }}
		</div>
		<div class="form-group">
			{{ Form::submit('Create task', array('class' => 'btn btn-primary')) }}
		</div>
	</fieldset>
{{ Form::close() }}