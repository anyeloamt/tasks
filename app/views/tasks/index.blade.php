@extends('layouts.master')

@section('content')

<div class="col-md-{{ isset($users) ? '6' : '6 col-md-offset-3' }}">
	<h1>All tasks</h1>
	<ul class="list-group">
		@foreach ($tasks as $k => $task)
			<li class="list-group-item {{ $task->completed ? 'completed' : '' }}">
				{{ task_tag($task) }}
				{{ Form::model($task, 
						array(
							'class' => 'task-update-form', 
							'method' => 'PATCH', 
							'route' => array(
								'task.update', $task->id
							)
						)
					) 
				}}
					{{ Form::checkbox('completed') }}
					{{ Form::submit('Update', array('class' => 'btn btn-xs btn-primary')) }}
					<img src="/img/AjaxLoader.gif" alt="Cargando..." class="hidden" id="loader-{{ $k }}">
				{{ Form::close() }}
			</li>
		@endforeach
	</ul>
</div>

@if (isset($users))
	<div class="col-md-6">
		<h3>Add new task</h3>
		@include('tasks.partials._form')
	</div>
@endif

@stop