<?php

Event::listen('illuminate.query', function($query)
{
	//var_dump($query);
});

Route::patch('/task/{id}', array('as' => 'task.update', 'uses' => 'TasksController@update'));

Route::post('/tasks', 'TasksController@store');

Route::get('/', array('as' => 'home', 'uses' => 'TasksController@index'));

Route::get('tasks/{id}', 'TasksController@show')->where('id', '\d+');

Route::get('/{username}/tasks', 'UsersTasksController@index');

Route::get('{username}/tasks/{id}', array('as' => 'user.tasks.show', 'uses' => 'UsersTasksController@show'));