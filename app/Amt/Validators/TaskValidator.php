<?php namespace Amt\Validators;

class TaskValidator extends Validator
{
	protected static $rules = array(
		'title' => 'required',
		'body' => 'required',
	);
}