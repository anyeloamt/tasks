<?php namespace Amt\Services;

use Amt\Validators\TaskValidator;
use Amt\Validators\ValidationException;
use Task;

/**
 * Task creator service to create tasks
 */
class TaskCreatorService
{
	/**
	 * Service validator
	 * @var \Validator
	 */
	protected $validator;

	public function __construct(TaskValidator $validator)
	{
		$this->validator = $validator;
	}
	
	public function make(array $attributes)
	{
		// Determine wheter the data is valid
		if ($this->validator->isValid($attributes))
		{
			// Create a task
			Task::create(array(
				'title' => $attributes['title'],
				'body' => $attributes['body'],
				'user_id' => $attributes['assign']
			));	

			return true;
		}
		// If not throw an exception
		throw new ValidationException('Task validations fails', $this->validator->getErrors());
	}
}